package org.dromara.northstar.gateway.ctp.x64v6v3v15v;

import static xyz.redtorch.pb.CoreEnum.ExchangeEnum.INE;
import static xyz.redtorch.pb.CoreEnum.ExchangeEnum.SHFE;

import org.dromara.northstar.common.event.FastEventEngine;
import org.dromara.northstar.common.model.GatewayDescription;
import org.dromara.northstar.common.model.core.SubmitOrderReq;
import org.dromara.northstar.gateway.TradeGateway;
import org.dromara.northstar.gateway.mktdata.MarketCenter;

import lombok.extern.slf4j.Slf4j;
import xyz.redtorch.pb.CoreEnum.ExchangeEnum;
import xyz.redtorch.pb.CoreEnum.OrderPriceTypeEnum;
import xyz.redtorch.pb.CoreEnum.ProductClassEnum;

@Slf4j
class CtpTradeGateway extends TradeGateway{
	
	private final TdSpi tdSpi;
	
	CtpTradeGateway(GatewayDescription description, FastEventEngine feEngine, MarketCenter mktCenter) {
		super(description);
		tdSpi = new TdSpi(this, feEngine, mktCenter);
	}

	@Override
	public String submitOrder(SubmitOrderReq submitOrderReq) {
		if (!tdSpi.isConnected()) {
			throw new IllegalStateException("网关未连线");
		}
		ExchangeEnum exchange = submitOrderReq.contract().exchange();
		ProductClassEnum productClass = submitOrderReq.contract().productClass();
		boolean nonAnyPrice = productClass == ProductClassEnum.FUTURES && (exchange == SHFE || exchange == INE)
				|| productClass == ProductClassEnum.OPTION && exchange != ExchangeEnum.CZCE;
		if(submitOrderReq.orderPriceType() == OrderPriceTypeEnum.OPT_AnyPrice && nonAnyPrice) {
			log.info("{} 交易所不支持{}合约市价单，将自动转换为限价单", exchange, productClass);
			return tdSpi.submitOrder(submitOrderReq.toBuilder().orderPriceType(OrderPriceTypeEnum.OPT_LimitPrice).build());
		}
		
		return tdSpi.submitOrder(submitOrderReq);
	}

	@Override
	public boolean cancelOrder(String originOrderId) {
		if (!tdSpi.isConnected()) {
			throw new IllegalStateException("网关未连线");
		}
		return tdSpi.cancelOrder(originOrderId);
	}

	@Override
	public void connect() {
		tdSpi.connect();
	}

	@Override
	public void disconnect() {
		tdSpi.disconnect();
	}

}
