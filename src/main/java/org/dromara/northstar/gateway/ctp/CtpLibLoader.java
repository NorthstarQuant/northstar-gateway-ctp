package org.dromara.northstar.gateway.ctp;

import java.io.File;
import java.util.Objects;

import org.dromara.northstar.common.constant.Platform;
import org.dromara.northstar.common.utils.CommonUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * 负责唯一加载ctp库，避免同时加载生产库与仿真库时出现异常
 */
@Slf4j
public class CtpLibLoader {
	
	private static Boolean isProd;
	
	private CtpLibLoader() {}

	public static void loadProdLib() {
		if(Objects.nonNull(isProd)) {
			if(Boolean.TRUE.equals(isProd)) {
				return;
			} else {
				throw new IllegalStateException("已经加载CTP仿真库，无法继续加载生产库");
			}
		}
		
		String envTmpDir = "";
		String tempLibPath = "";
		try {
			switch(Platform.current()) {
				case WINDOWS -> {
					log.info("开始复制（生产）运行库");
					envTmpDir = System.getProperty("java.io.tmpdir");
					tempLibPath = envTmpDir + File.separator + "xyz" + File.separator + "redtorch" + File.separator + "api" + File.separator + "jctp" + File.separator + "lib" + File.separator
							+ "jctpv6v3v15x64api" + File.separator;
	
					CommonUtils.copyURLToFileForTmp(tempLibPath, CtpLibLoader.class.getResource("/assembly/libiconv.dll"));
					CommonUtils.copyURLToFileForTmp(tempLibPath, CtpLibLoader.class.getResource("/assembly/jctpv6v3v15x64api/thostmduserapi_se.dll"));
					CommonUtils.copyURLToFileForTmp(tempLibPath, CtpLibLoader.class.getResource("/assembly/jctpv6v3v15x64api/thosttraderapi_se.dll"));
					CommonUtils.copyURLToFileForTmp(tempLibPath, CtpLibLoader.class.getResource("/assembly/jctpv6v3v15x64api/jctpv6v3v15x64api.dll"));
					
					log.info("开始加载（生产）运行库");
					System.load(tempLibPath + File.separator + "libiconv.dll");
					System.load(tempLibPath + File.separator + "thostmduserapi_se.dll");
					System.load(tempLibPath + File.separator + "thosttraderapi_se.dll");
					System.load(tempLibPath + File.separator + "jctpv6v3v15x64api.dll");
				}
				case LINUX -> {
					log.info("开始复制（生产）运行库");
					envTmpDir = "/tmp";
					tempLibPath = envTmpDir + File.separator + "xyz" + File.separator + "redtorch" + File.separator + "api" + File.separator + "jctp" + File.separator + "lib" + File.separator
							+ "jctpv6v3v15x64api" + File.separator;
	
					CommonUtils.copyURLToFileForTmp(tempLibPath, CtpLibLoader.class.getResource("/assembly/jctpv6v3v15x64api/libthostmduserapi_se.so"));
					CommonUtils.copyURLToFileForTmp(tempLibPath, CtpLibLoader.class.getResource("/assembly/jctpv6v3v15x64api/libthosttraderapi_se.so"));
					CommonUtils.copyURLToFileForTmp(tempLibPath, CtpLibLoader.class.getResource("/assembly/jctpv6v3v15x64api/libjctpv6v3v15x64api.so"));
					
					log.info("开始加载（生产）运行库");
					System.load(tempLibPath + File.separator + "libthostmduserapi_se.so");
					System.load(tempLibPath + File.separator + "libthosttraderapi_se.so");
					System.load(tempLibPath + File.separator + "libjctpv6v3v15x64api.so");
				}
				default -> throw new IllegalArgumentException("Unexpected value: " + Platform.current());
			}
		} catch (Exception e) {
			log.warn("（生产）运行库加载失败", e);
		}
		
		isProd = Boolean.TRUE;
	}
	
	public static void loadSimLib() {
		if(Objects.nonNull(isProd)) {
			if(Boolean.FALSE.equals(isProd)) {
				return;
			} else {
				throw new IllegalStateException("已经加载CTP生产库，无法继续加载仿真库");
			}
		}
		String envTmpDir = "";
		String tempLibPath = "";
		try {
			switch(Platform.current()) {
				case WINDOWS -> {
					log.info("开始复制（仿真）运行库");
					envTmpDir = System.getProperty("java.io.tmpdir");
					tempLibPath = envTmpDir + File.separator + "xyz" + File.separator + "redtorch" + File.separator + "api" + File.separator + "jctp" + File.separator + "lib" + File.separator
							+ "jctpv6v5v1cpx64api" + File.separator;
	
					CommonUtils.copyURLToFileForTmp(tempLibPath, CtpLibLoader.class.getResource("/assembly/libiconv.dll"));
					CommonUtils.copyURLToFileForTmp(tempLibPath, CtpLibLoader.class.getResource("/assembly/jctpv6v5v1cpx64api/thostmduserapi_se.dll"));
					CommonUtils.copyURLToFileForTmp(tempLibPath, CtpLibLoader.class.getResource("/assembly/jctpv6v5v1cpx64api/thosttraderapi_se.dll"));
					CommonUtils.copyURLToFileForTmp(tempLibPath, CtpLibLoader.class.getResource("/assembly/jctpv6v5v1cpx64api/jctpv6v5v1cpx64api.dll"));
					
					log.info("开始加载（仿真）运行库");
					System.load(tempLibPath + File.separator + "libiconv.dll");
					System.load(tempLibPath + File.separator + "thostmduserapi_se.dll");
					System.load(tempLibPath + File.separator + "thosttraderapi_se.dll");
					System.load(tempLibPath + File.separator + "jctpv6v5v1cpx64api.dll");
				}
				case LINUX -> {
					log.info("开始复制（仿真）运行库");
					envTmpDir = "/tmp";
					tempLibPath = envTmpDir + File.separator + "xyz" + File.separator + "redtorch" + File.separator + "api" + File.separator + "jctp" + File.separator + "lib" + File.separator
							+ "jctpv6v5v1cpx64api" + File.separator;
	
					CommonUtils.copyURLToFileForTmp(tempLibPath, CtpLibLoader.class.getResource("/assembly/jctpv6v5v1cpx64api/libthostmduserapi_se.so"));
					CommonUtils.copyURLToFileForTmp(tempLibPath, CtpLibLoader.class.getResource("/assembly/jctpv6v5v1cpx64api/libthosttraderapi_se.so"));
					CommonUtils.copyURLToFileForTmp(tempLibPath, CtpLibLoader.class.getResource("/assembly/jctpv6v5v1cpx64api/libjctpv6v5v1cpx64api.so"));
					
					log.info("开始加载（仿真）运行库");
					System.load(tempLibPath + File.separator + "libthostmduserapi_se.so");
					System.load(tempLibPath + File.separator + "libthosttraderapi_se.so");
					System.load(tempLibPath + File.separator + "libjctpv6v5v1cpx64api.so");
				}
				default -> throw new IllegalArgumentException("Unexpected value: " + Platform.current());
			}
		} catch (Exception e) {
			log.warn("（仿真）运行库加载失败", e);
		}
		
		isProd = Boolean.FALSE;
	}
	
}
