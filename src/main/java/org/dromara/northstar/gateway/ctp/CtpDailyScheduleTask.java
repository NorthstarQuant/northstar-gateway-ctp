package org.dromara.northstar.gateway.ctp;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.dromara.northstar.common.ObjectManager;
import org.dromara.northstar.common.constant.ChannelType;
import org.dromara.northstar.common.constant.ConnectionState;
import org.dromara.northstar.gateway.Gateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class CtpDailyScheduleTask {

	@Autowired
	private ObjectManager<Gateway> gatewayMgr;
	
	@Autowired
	private CtpHolidayManager holidayMgr;
	
	/**
	 * 网关开盘前连线检查
	 * @throws InterruptedException
	 */
	@Scheduled(cron="0 55 8,20 ? * 1-5")
	public void dailyConnection() throws InterruptedException {
		if(holidayMgr.isHoliday(LocalDateTime.now())) {
			log.info("当前为假期，不执行定时连线任务");
			return;
		}
		log.info("定时连线任务");
		connectIfNotConnected();
	}
	/**
	 * 网关定时断开
	 * @throws InterruptedException
	 */
	@Scheduled(cron="0 31 2,15 ? * 1-6")
	public void dailyDisconnection() throws InterruptedException {
		if(holidayMgr.isHoliday(LocalDateTime.now()) && LocalDate.now().getDayOfWeek() != DayOfWeek.SATURDAY) {
			log.info("当前为假期，不执行定时离线任务");
			return;
		}
		log.info("定时离线任务");
		gatewayMgr.findAll().stream()
			.filter(gw -> gw.getDescription().getChannelType() == ChannelType.CTP)
			.filter(gw -> gw.getConnection() == ConnectionState.CONNECTED)
			.forEach(gw -> {
				// 确保网关是逐个断开
				gw.disconnect();
				int i = 0;
				while(i++<100 && gw.getConnection() != ConnectionState.DISCONNECTED) {
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
						log.error("", e);
					}
				}
			});
	}

	/**
	 * 自动连线任务
	 */
	@Scheduled(cron="0 0/1 0-2,9-15,21-23 * * 1-6")
	public void timelyCheckConnection() {
		// 以国债与沪金的交易时间为基准计算自动连线
		if(holidayMgr.isHoliday(LocalDateTime.now())) {
			log.debug("当前为假期，不执行开盘连线巡检任务");
			return;
		}
		if(!withTradeTime()) {
			log.debug("不是交易时段，忽略连线");
			return;
		}
		log.debug("开盘时间连线巡检");
		connectIfNotConnected();
	}
	
	private static LocalTime NIGHT_OPEN = LocalTime.of(21, 0);
	private static LocalTime NIGHT_END = LocalTime.of(2, 30);
	private static LocalTime DAY_OPEN = LocalTime.of(9, 0);
	private static LocalTime DAY_END = LocalTime.of(15, 15);
	private boolean withTradeTime() {
		LocalTime now = LocalTime.now();
		return now.isAfter(NIGHT_OPEN) || now.isBefore(NIGHT_END) || now.isAfter(DAY_OPEN) && now.isBefore(DAY_END);
	}
	
	private void connectIfNotConnected() {
		gatewayMgr.findAll().stream()
			.filter(gw -> gw.getDescription().getChannelType() == ChannelType.CTP)
			.filter(gw -> gw.getDescription().isAutoConnect())
			.filter(gw -> gw.getConnection() != ConnectionState.CONNECTED)
			.forEach(Gateway::connect);
	}
}
