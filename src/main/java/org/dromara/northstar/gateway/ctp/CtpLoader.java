package org.dromara.northstar.gateway.ctp;

import java.time.LocalDate;
import java.util.Set;
import java.util.stream.Collectors;

import org.dromara.northstar.common.constant.ChannelType;
import org.dromara.northstar.common.constant.GatewayUsage;
import org.dromara.northstar.common.model.ContractSimpleInfo;
import org.dromara.northstar.data.IGatewayRepository;
import org.dromara.northstar.data.IModuleRepository;
import org.dromara.northstar.gateway.IMarketCenter;
import org.dromara.northstar.gateway.ctp.x64v6v3v15v.CtpGatewayFactory;
import org.dromara.northstar.gateway.ctp.x64v6v5v1cpv.CtpSimGatewayFactory;
import org.dromara.northstar.gateway.mktdata.NorthstarDataSource;
import org.dromara.northstar.gateway.mktdata.QuantitDataServiceManager;
import org.dromara.northstar.meta.GatewayMetaProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Order(0)	// 加载顺序需要显式声明，否则会最后才被加载，从而导致加载网关与模组时报异常
@Component
public class CtpLoader implements CommandLineRunner{
	
	@Autowired
	private IMarketCenter mktCenter;
	
	@Autowired
	private QuantitDataServiceManager dsMgr;
	
	@Autowired
	private GatewayMetaProvider gatewayMetaProvider;
	
	@Autowired
	private CtpGatewayFactory ctpFactory;
	
	@Autowired
	private CtpSimGatewayFactory ctpSimFactory;
	
	@Autowired
	private CtpContractDefProvider contractDefPvd;
	
	@Autowired
	private IGatewayRepository gatewayRepo;
	
	@Autowired
	private IModuleRepository moduleRepo;
	
	@Override
	public void run(String... args) throws Exception {
		gatewayMetaProvider.register(ChannelType.CTP, new CtpGatewaySettings(), ctpFactory);
		gatewayMetaProvider.register(ChannelType.CTP_SIM, new CtpSimGatewaySettings(), ctpSimFactory);
		
		mktCenter.addDefinitions(contractDefPvd.get());
		final LocalDate today = LocalDate.now();
		
		NorthstarDataSource ds = new NorthstarDataSource(dsMgr, NorthstarDataSource.CN_FUTURES);
		log.info("加载CTP期货合约");
		
		Set<String> subscribed = gatewayRepo.findAll().stream()
				.filter(gd -> gd.getGatewayUsage() == GatewayUsage.MARKET_DATA)
				.flatMap(gd -> gd.getSubscribedContracts().stream())
				.map(ContractSimpleInfo::getUnifiedSymbol)
				.collect(Collectors.toSet());
		Set<String> binded = moduleRepo.findAllSettings().stream()
				.flatMap(md -> md.getBindedContracts().stream())
				.map(ContractSimpleInfo::getUnifiedSymbol)
				.collect(Collectors.toSet());
		// 加载CTP合约
		ds.getAllContracts().stream()
			//过滤掉过期合约
			.filter(c -> c.lastTradeDate().isAfter(today) || subscribed.contains(c.unifiedSymbol()) || binded.contains(c.unifiedSymbol()))
			.forEach(contract -> mktCenter.addInstrument(new CtpContract(contract, ds)));
		mktCenter.loadContractGroup(ChannelType.CTP);
	}

}
