package org.dromara.northstar.gateway.ctp;

public class CtpConstants {

	/**
	 * CTP期货公司网关定义文件的环境变量
	 */
	public static final String CTP_CHANNEL_FILE = "NS_CTP_CHANNEL_FILE";
}
