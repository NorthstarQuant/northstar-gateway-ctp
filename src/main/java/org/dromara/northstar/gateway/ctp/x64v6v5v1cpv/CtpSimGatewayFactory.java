package org.dromara.northstar.gateway.ctp.x64v6v5v1cpv;

import org.dromara.northstar.common.constant.GatewayUsage;
import org.dromara.northstar.common.event.FastEventEngine;
import org.dromara.northstar.common.model.GatewayDescription;
import org.dromara.northstar.gateway.Gateway;
import org.dromara.northstar.gateway.GatewayFactory;
import org.dromara.northstar.gateway.ctp.CtpLibLoader;
import org.dromara.northstar.gateway.ctp.CtpSimGatewaySettings;
import org.dromara.northstar.gateway.mktdata.MarketCenter;

import com.alibaba.fastjson2.JSON;

public class CtpSimGatewayFactory implements GatewayFactory{

	private FastEventEngine fastEventEngine;
	private MarketCenter mktCenter;

	public CtpSimGatewayFactory(FastEventEngine fastEventEngine, MarketCenter mktCenter) {
		this.fastEventEngine = fastEventEngine;
		this.mktCenter = mktCenter;
	}

	@Override
	public Gateway newInstance(GatewayDescription gatewayDescription) {
		GatewayDescription gd = enhance(gatewayDescription);
		CtpLibLoader.loadSimLib();
		if(gatewayDescription.getGatewayUsage() == GatewayUsage.MARKET_DATA) {
			return new CtpSimMarketGateway(gd, fastEventEngine, mktCenter);
		}
		return new CtpSimTradeGateway(gd, fastEventEngine, mktCenter);
	}

	@Override
	public GatewayDescription enhance(GatewayDescription gatewayDescription) {
		CtpSimGatewaySettings settings = JSON.parseObject(JSON.toJSONString(gatewayDescription.getSettings()), CtpSimGatewaySettings.class);
		gatewayDescription.setSettings(settings);
		return gatewayDescription;
	}

}
