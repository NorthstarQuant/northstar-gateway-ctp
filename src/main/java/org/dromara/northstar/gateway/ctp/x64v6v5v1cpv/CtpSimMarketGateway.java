package org.dromara.northstar.gateway.ctp.x64v6v5v1cpv;

import org.dromara.northstar.common.event.FastEventEngine;
import org.dromara.northstar.common.model.GatewayDescription;
import org.dromara.northstar.common.model.core.Contract;
import org.dromara.northstar.gateway.MarketGateway;
import org.dromara.northstar.gateway.mktdata.MarketCenter;

import lombok.extern.slf4j.Slf4j;

@Slf4j
class CtpSimMarketGateway extends MarketGateway{
	
	private MdSpi mdSpi;

	CtpSimMarketGateway(GatewayDescription description, FastEventEngine fastEventEngine, MarketCenter mktCenter) {
		super(description);
		mdSpi = new MdSpi(this, fastEventEngine, mktCenter);
	}

	@Override
	public boolean isActive() {
		return mdSpi.isActive();
	}

	@Override
	public boolean subscribe(Contract contract) {
		if (mdSpi == null) {
			log.error("{} 行情接口尚未初始化或已断开", description.getGatewayId());
			return false;
		}
		return mdSpi.subscribe(contract.symbol());
	}

	@Override
	public boolean unsubscribe(Contract contract) {
		if (mdSpi == null) {
			log.error("{} 行情接口尚未初始化或已断开", description.getGatewayId());
			return false;
		}
		return mdSpi.unsubscribe(contract.symbol());
	}

	@Override
	public void connect() {
		mdSpi.connect();
	}

	@Override
	public void disconnect() {
		mdSpi.disconnect();
	}

}
