package org.dromara.northstar.gateway.ctp.x64v6v3v15v;

import java.util.Map;
import java.util.stream.Collectors;

import org.dromara.northstar.common.constant.GatewayUsage;
import org.dromara.northstar.common.event.FastEventEngine;
import org.dromara.northstar.common.model.GatewayDescription;
import org.dromara.northstar.gateway.Gateway;
import org.dromara.northstar.gateway.GatewayFactory;
import org.dromara.northstar.gateway.ctp.CtpGatewayChannelProvider;
import org.dromara.northstar.gateway.ctp.CtpGatewaySettings;
import org.dromara.northstar.gateway.ctp.CtpGatewaySettings.Broker;
import org.dromara.northstar.gateway.ctp.CtpLibLoader;
import org.dromara.northstar.gateway.mktdata.MarketCenter;

import com.alibaba.fastjson2.JSON;

public class CtpGatewayFactory implements GatewayFactory{

	private FastEventEngine fastEventEngine;
	
	private MarketCenter mktCenter;
	
	private Map<String, Broker> brokerMap;
	
	public CtpGatewayFactory(FastEventEngine fastEventEngine, MarketCenter mktCenter) {
		this.brokerMap = new CtpGatewayChannelProvider().brokerList().stream().collect(Collectors.toMap(Broker::getName, b -> b));
		this.fastEventEngine = fastEventEngine;
		this.mktCenter = mktCenter;
	}
	
	@Override
	public Gateway newInstance(GatewayDescription gatewayDescription) {
		GatewayDescription gd = enhance(gatewayDescription);
		CtpGatewaySettings settings = (CtpGatewaySettings) gd.getSettings();
		if(!brokerMap.containsKey(settings.getBrokerName())) {
			throw new IllegalStateException("没有找到期货公司信息：" + settings.getBrokerName());
		}
		settings.setBroker(brokerMap.get(settings.getBrokerName()));
		CtpLibLoader.loadProdLib();
		if(gatewayDescription.getGatewayUsage() == GatewayUsage.MARKET_DATA) {
			return new CtpMarketGateway(gd, fastEventEngine, mktCenter);
		}
		return new CtpTradeGateway(gd, fastEventEngine, mktCenter);
	}

	@Override
	public GatewayDescription enhance(GatewayDescription gatewayDescription) {
		CtpGatewaySettings settings = JSON.parseObject(JSON.toJSONString(gatewayDescription.getSettings()), CtpGatewaySettings.class);
		gatewayDescription.setSettings(settings);
		return gatewayDescription;
	}
	
}
